# 企业实际场景中应该根据企业自身情况构建统一的基础镜像
FROM openjdk:8-jre-slim
LABEL maintainer="dehua@rancher.com"
# 企业实际场景中应该通过USER指定以非root用户运行
# USER appuser
RUN echo "Asia/Shanghai" > /etc/timezone
EXPOSE 8080
COPY target/*.jar /app/
WORKDIR /app
CMD ["java", "-jar", "/app/spring-petclinic.jar"]